# Put data files here

Get data from [crates.rs/data](https://crates.rs/data) and put them here (i.e. `./data/` directory in the [crates.rs repo](https://gitlab.com/crates.rs/crates.rs)),
